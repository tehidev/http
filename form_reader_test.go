package http

import (
	"io/ioutil"
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFormReader(t *testing.T) {
	testBody := "------WebKitFormBoundarytPStCsmX6BTb661o\n" +
		"Content-Disposition: form-data; name=\"string1\"\n" +
		"\n" +
		"value1\n" +
		"------WebKitFormBoundarytPStCsmX6BTb661o\n" +
		"Content-Disposition: form-data; name=\"string2\"\n" +
		"\n" +
		"value2\n" +
		"------WebKitFormBoundarytPStCsmX6BTb661o\n" +
		"Content-Disposition: form-data; name=\"file\"; filename=\"213.txt\"\n" +
		"Content-Type: text/plain\n" +
		"\n" +
		"231\n" +
		"ew\n" +
		"wg\n" +
		"ww\n" +
		"g\n" +
		"wegewgwgwgweg\n" +
		"ewgwe\n" +
		"------WebKitFormBoundarytPStCsmX6BTb661o--\n" +
		""

	req, err := http.NewRequest("POST", "/", strings.NewReader(testBody))
	assert.NoError(t, err)
	req.ContentLength = int64(len(testBody))
	f := NewFormReader(req)

	expectedFileBody := "231\new\nwg\nww\ng\nwegewgwgwgweg\newgwe"

	test := func() {
		assert.True(t, f.NextField())
		assert.False(t, f.IsFile)
		assert.Equal(t, "string1", f.Name)
		assert.Equal(t, "value1", f.ReadString())

		assert.True(t, f.NextField())
		assert.False(t, f.IsFile)
		assert.Equal(t, "string2", f.Name)
		assert.Equal(t, "value2", f.ReadString())

		assert.True(t, f.NextField())
		assert.True(t, f.IsFile)
		assert.Equal(t, "file", f.Name)
		assert.Equal(t, "213.txt", f.Filename)
		assert.Equal(t, "text/plain", f.ContentType)

		expectedSize := f.Remaining()

		fileBody, err := ioutil.ReadAll(f)
		assert.NoError(t, err)
		assert.Equal(t, expectedFileBody, string(fileBody))

		assert.Equal(t, int(expectedSize), len(fileBody))

		assert.False(t, f.NextField())
	}

	test()

	req, _ = http.NewRequest("POST", "/", strings.NewReader(testBody))
	req.ContentLength = int64(len(testBody))
	f.Reset(req)

	test()

	testBody = strings.ReplaceAll(testBody, "\n", "\r\n")
	expectedFileBody = strings.ReplaceAll(expectedFileBody, "\n", "\r\n")
	req, _ = http.NewRequest("POST", "/", strings.NewReader(testBody))
	req.ContentLength = int64(len(testBody))
	f.Reset(req)

	test()
}
