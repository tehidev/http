package http

import (
	"bufio"
	"bytes"
	"io"
	"io/ioutil"
	"net/http"
	"sync"

	"gitlab.com/knopkalab/go/utils"
)

var formReaderPool = &sync.Pool{New: func() interface{} { return new(FormReader) }}

// GetFormReader from pool and fill
func GetFormReader(req *http.Request) *FormReader {
	fr := formReaderPool.Get().(*FormReader)
	fr.Reset(req)
	return fr
}

// PutFormReader to pool
func PutFormReader(fr *FormReader) {
	formReaderPool.Put(fr)
}

// FormReader for multipart fields
type FormReader struct {
	body      *bufio.Reader
	boundary  []byte
	crlf      bool
	remaining int64

	buf []byte
	off int

	IsFile      bool
	Name        string
	Filename    string
	ContentType string
}

// NewFormReader for multipart fields
func NewFormReader(r *http.Request) *FormReader {
	reader := new(FormReader)
	reader.Reset(r)
	return reader
}

// Reset request form
func (r *FormReader) Reset(req *http.Request) {
	if req == nil {
		return
	}
	if r.body == nil {
		r.body = bufio.NewReader(req.Body)
	} else {
		r.body.Reset(req.Body)
	}
	if r.buf == nil {
		r.buf = make([]byte, 4096)
	}
	r.off = 0
	r.boundary = nil
	r.remaining = req.ContentLength
}

var (
	formAttrContentType    = []byte("Content-Type: ")
	formAttrContentTypeLen = len(formAttrContentType)
	formAttrName           = []byte(`name="`)
	formAttrNameLen        = len(formAttrName)
	formAttrFilename       = []byte(`filename="`)
	formAttrFilenameLen    = len(formAttrFilename)
	formEndingLF           = []byte("\n")
	formEndingCRLF         = []byte("\r\n")
)

func formLineEndingIndex(line []byte) int {
	for i := len(line); i > 0; i-- {
		if c := line[i-1]; c != '\r' && c != '\n' {
			return i
		}
	}
	return 0
}

// NextField parsed field header if available
func (r *FormReader) NextField() bool {
	if r.boundary == nil {
		line, err := r.body.ReadSlice('\n')
		if err != nil {
			return false
		}
		r.remaining -= int64(len(line))
		i := formLineEndingIndex(line)
		r.boundary = make([]byte, i)
		copy(r.boundary, line[:i])
	}
	r.crlf = false
	r.IsFile = false
	r.Name = ""
	r.Filename = ""
	r.ContentType = ""
	for iter := 0; iter < 10; iter++ {
		line, err := r.body.ReadSlice('\n')
		if err != nil {
			return false
		}
		r.remaining -= int64(len(line))
		if bytes.HasPrefix(line, formAttrContentType) {
			r.ContentType = string(line[formAttrContentTypeLen:formLineEndingIndex(line)])
			continue
		}
		nameIdx := bytes.Index(line, formAttrName)
		if nameIdx != -1 {
			nameIdx += formAttrNameLen
			nameEndIdx := bytes.IndexRune(line[nameIdx:], '"')
			if nameEndIdx == -1 {
				return false
			}
			r.Name = string(line[nameIdx : nameIdx+nameEndIdx])
		}
		filenameIdx := bytes.Index(line, formAttrFilename)
		if filenameIdx != -1 {
			filenameIdx += formAttrFilenameLen
			filenameEndIdx := bytes.IndexRune(line[filenameIdx:], '"')
			if filenameEndIdx == -1 {
				return false
			}
			r.Filename = string(line[filenameIdx : filenameIdx+filenameEndIdx])
			r.IsFile = true
		}
		switch {
		case bytes.Equal(line, formEndingCRLF):
			r.crlf = true
			fallthrough
		case bytes.Equal(line, formEndingLF):
			return true
		}
	}
	return false
}

// Skip field data
func (r *FormReader) Skip() (n int64) {
	n, _ = io.Copy(ioutil.Discard, r)
	return
}

// Remaining bytes count
func (r *FormReader) Remaining() int64 {
	count := r.remaining - int64(len(r.boundary)) - 2 - 2 // boundary '--' + '\n\n'
	if r.crlf {
		count -= 2 // '\r\r'
	}
	if count < 0 {
		return 0
	}
	return count
}

func (r *FormReader) Read(b []byte) (n int, _ error) {
	line, err := r.body.ReadSlice('\n')
	if err != nil && err != bufio.ErrBufferFull {
		return 0, err
	}
	r.remaining -= int64(len(line))
	if bytes.HasPrefix(line, r.boundary) {
		if r.crlf {
			n = copy(b, r.buf[:r.off-2])
		} else {
			n = copy(b, r.buf[:r.off-1])
		}
		r.off = 0
		return n, io.EOF
	}
	n = copy(b, r.buf[:r.off])
	r.off = copy(r.buf, line)
	return n, nil
}

// ReadBytes of field value
func (r *FormReader) ReadBytes() ([]byte, error) {
	return ioutil.ReadAll(r)
}

// ReadString of field value
func (r *FormReader) ReadString() string {
	if body, err := r.ReadBytes(); err == nil {
		return utils.ZBtoa(body)
	}
	return ""
}
