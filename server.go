package http

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"strings"
	"time"

	"gitlab.com/knopkalab/go/utils"
	"gitlab.com/knopkalab/tracer"
)

type Server struct {
	span tracer.Span
	serv *http.Server

	listenHost string
	listenPort uint16
}

func NewServer() *Server {
	s := &Server{
		listenHost: "0.0.0.0",
		serv: &http.Server{
			ReadTimeout:  time.Second * 20,
			WriteTimeout: time.Second * 20,
			IdleTimeout:  time.Second * 5,
			ErrorLog:     utils.NewDiscardLogger(),
		},
	}
	return s
}

func (s *Server) WithPort(listenPort uint16) *Server {
	s.listenPort = listenPort
	return s
}

func (s *Server) WithHost(listenHost string) *Server {
	s.listenHost = listenHost
	return s
}

func (s *Server) WithHandler(h http.Handler) *Server {
	s.serv.Handler = h
	return s
}

func (s *Server) WithErrorLog(logger *log.Logger) *Server {
	s.serv.ErrorLog = logger
	return s
}

func (s *Server) WithTimeouts(readTimeout, writeTimeout, idleTimeout time.Duration) *Server {
	s.serv.ReadTimeout = readTimeout
	s.serv.WriteTimeout = writeTimeout
	s.serv.IdleTimeout = idleTimeout
	return s
}

func (s *Server) Close() error {
	defer s.span.Finish()
	err := s.serv.Shutdown(context.TODO())
	if err != nil {
		if err == http.ErrServerClosed {
			s.span.Info().Msg("server closed")
		} else {
			s.span.Warn().Err(err).Msg("closing failed")
		}
	}
	return err
}

func (s *Server) Start(ctx context.Context) error {
	ctx, span := tracer.Start(ctx, "http_server")
	s.span = span

	listenAddr := fmt.Sprintf("%s:%d", s.listenHost, s.listenPort)
	span.Debug().
		Str("listen_addr", listenAddr).
		Msg("open TCP socket")
	socket, err := net.Listen("tcp", listenAddr)
	if err != nil {
		span.Error(err).
			Str("listen_addr", listenAddr).
			Msg("cannot to open TCP socket")
		span.Finish()
		return err
	}

	listenAddr = socket.Addr().String()
	if strings.HasPrefix(listenAddr, "[::]") {
		listenAddr = "127.0.0.1" + listenAddr[len("[::]"):]
	}

	ctx = tracer.StartSpanContext(ctx, "requests")
	originHandler := s.serv.Handler

	if originHandler != nil {
		s.serv.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			originHandler.ServeHTTP(w, r.WithContext(
				tracer.WithContext(r.Context(), ctx),
			))
		})
	}

	begin := make(chan struct{})
	go func() {
		close(begin)
		span.Debug().Msgf("listen http://%s", listenAddr)
		err = s.serv.Serve(socket)
		if err != nil && err != http.ErrServerClosed {
			span.Error(err).Msg("listen error")
		} else {
			span.Debug().Msg("server closed")
		}
	}()
	<-begin

	return err
}
