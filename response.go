package http

import (
	"bufio"
	htemplate "html/template"
	"net"
	"net/http"
	"strconv"
	ttemplate "text/template"

	jsoniter "github.com/json-iterator/go"

	"gitlab.com/knopkalab/go/utils"
)

type Response struct {
	http.ResponseWriter
	responseCode int
	responseSize int
}

func (r *Response) ResponseCode() int { return r.responseCode }

func (r *Response) ResponseSize() int { return r.responseSize }

// Flush implements the http.Flusher interface to allow an HTTP handler to flush buffered data to the client
func (r *Response) Flush() {
	r.ResponseWriter.(http.Flusher).Flush()
}

// Hijack implements the http.Hijacker interface to allow an HTTP handler to take over the connection
func (r *Response) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	return r.ResponseWriter.(http.Hijacker).Hijack()
}

func (r *Response) WriteHeader(statusCode int) {
	if r.responseCode == 0 {
		r.ResponseWriter.WriteHeader(statusCode)
		r.responseCode = statusCode
	}
}

func (r *Response) Write(data []byte) (int, error) {
	r.WriteHeader(StatusOK)
	n, err := r.ResponseWriter.Write(data)
	r.responseSize += n
	return n, err
}

func (r *Response) SetCookie(cookie *Cookie) {
	http.SetCookie(r, cookie)
}

func (r *Response) Status(code int) error {
	if code == StatusNoContent {
		r.WriteHeader(StatusNoContent)
		return nil
	}
	return r.Bytes(code, StatusBytes(code))
}

func (r *Response) StatusOK() error { return r.Status(StatusOK) }

func (r *Response) StatusCreated() error { return r.Status(StatusCreated) }

func (r *Response) StatusNoContent() error { return r.Status(StatusNoContent) }

func (r *Response) StatusNotFound() error { return r.Status(StatusNotFound) }

func (r *Response) StatusForbidden() error { return r.Status(StatusForbidden) }

func (r *Response) StatusUnauthorized() error { return r.Status(StatusUnauthorized) }

func (r *Response) StatusTooLarge() error { return r.Status(StatusRequestEntityTooLarge) }

func (r *Response) StatusBadRequest() error { return r.Status(StatusBadRequest) }

func (r *Response) StatusConflict() error { return r.Status(StatusConflict) }

func (r *Response) Bytes(status int, b []byte) (err error) {
	r.WriteHeader(status)
	_, err = r.Write(b)
	return
}

func (r *Response) String(status int, s string) error {
	r.Header().Set(HeaderContentType, MimeTextPlain)
	return r.Bytes(status, utils.ZAtob(s))
}

func (r *Response) Int(status int, v int) error {
	return r.String(status, strconv.Itoa(v))
}

func (r *Response) Int64(status int, v int64) error {
	return r.String(status, strconv.FormatInt(v, 10))
}

func (r *Response) JSON(status int, src interface{}) error {
	stream := jsoniter.ConfigDefault.BorrowStream(r)
	defer jsoniter.ConfigDefault.ReturnStream(stream)
	stream.WriteVal(src)

	r.Header().Set(HeaderContentType, MimeApplicationJSON)
	r.Header().Set(HeaderContentLength, strconv.Itoa(stream.Buffered()))
	r.WriteHeader(status)
	return stream.Flush()
}

func (r *Response) Redirect(toURL string) error {
	r.Header().Set(HeaderLocation, toURL)
	return r.Status(StatusFound)
}

func (r *Response) RedirectPermanent(toURL string) error {
	r.Header().Set(HeaderLocation, toURL)
	return r.Status(StatusMovedPermanently)
}

func (r *Response) HTMLString(code int, html string) error {
	r.Header().Set(HeaderContentType, MimeTextHTML)
	return r.String(code, html)
}

func (r *Response) HTMLBytes(code int, html []byte) error {
	r.Header().Set(HeaderContentType, MimeTextHTML)
	return r.Bytes(code, html)
}

func (r *Response) HTMLTemplate(code int, tmpl *htemplate.Template, data interface{}) error {
	r.Header().Set(HeaderContentType, MimeTextHTML)
	r.WriteHeader(code)
	return tmpl.Execute(r, data)
}

func (r *Response) TextTemplate(code int, tmpl *ttemplate.Template, data interface{}) error {
	r.Header().Set(HeaderContentType, MimeTextHTML)
	r.WriteHeader(code)
	return tmpl.Execute(r, data)
}
